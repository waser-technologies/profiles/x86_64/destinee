# Destinée

96Gb RAM -> AMD Ryzen Threadripper 2920X (24) @ 3.5GHz + 2x Nvidia TITAN RTX


## Getting started

Install `manjaro-tools` and `git`

```zsh
pamac install manjaro-tools-iso git
```

Clone manjaro's ISO profiles at `$HOME/iso-profile`.

```zsh
git clone https://gitlab.manjaro.org/profiles-and-settings/iso-profiles.git ~/iso-profiles
```

Clone this profle at `$HOME/iso-profiles/oem/destiny-oem-gnome`.


```zsh
git clone https://gitlab.com/waser-technologies/profiles/x86_64/destinee.git ~/iso-profiles/oem/destiny-oem-gnome
```

Build an ISO image.

```zsh
buildiso -p destiny-oem-gnome
```

## Download a pre-built ISO image

To reward sponsors for pledging at least $5/month, I've automated this process so that they can easely get started.

1. [Become a Sponsor](https://github.com/sponsors/wasertech)
2. [Download the latest ISO image](https://github.com/PredictiveSingularity/Destiny/releases)*

*You must be logged in as a GitHub Sponsor or you'll get a 404 error.
